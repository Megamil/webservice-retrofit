package net.megamil.webservicesretrofit;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Criado por Eduardo dos santos em 18/03/2018.
 * Megamil.net
 */

public interface ICrudUsuario {

    @GET("listar_usuario")
    Call<Usuarios> listarUsuarios();

    @POST("criar_usuario")
    Call<Usuarios> criarUsuario(@Body ObjUsuario usuario);

    @PUT("editar_usuario")
    Call<Usuarios> editarUsuario(@Body ObjUsuario usuario);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constantes.servidor)
            .client(Constantes.okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
