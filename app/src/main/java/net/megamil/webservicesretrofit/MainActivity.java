package net.megamil.webservicesretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.gson.Gson;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button buscarCep;
    Button listarUsuarios;
    Button novoUsuario;
    Button editarUsuario;
    String cep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cep = "07152390";

        buscarCep = findViewById(R.id.buscarCep);
        buscarCep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IBuscarEndereco ObjEndereco = IBuscarEndereco.retrofit.create(IBuscarEndereco.class);
                final Call<Endereco> call = ObjEndereco.getEndereco(cep);
                call.enqueue(new Callback<Endereco>() {
                    @Override
                    public void onResponse(Call<Endereco> call, Response<Endereco> response) {
                        System.out.println("Buscar CEP - onResponse "+response.code()+" Mensagem "+response.message());

                        if(response.code() == 200) {
                            System.out.println("Sucesso, buscou pelo cep "+cep);
                            System.out.println("Status: "+response.body().status+" Resultado: "+response.body().resultado);

                            System.out.println(response.body().endereco.exibir());

                        } else {

                            Gson gson = new Gson();
                            try {
                                Endereco errorResponse = gson.fromJson(response.errorBody().string(), Endereco.class);
                                System.out.println(errorResponse.status);
                                System.out.println(errorResponse.resultado);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<Endereco> call, Throwable t) {
                        System.out.println("onFailure");
                        try {
                            System.out.println(call.execute().body());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println(t);
                    }
                });

            }
        });


        listarUsuarios = findViewById(R.id.listarUsuarios);
        listarUsuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ICrudUsuario ObjUsuarios = ICrudUsuario.retrofit.create(ICrudUsuario.class);
                final Call<Usuarios> call = ObjUsuarios.listarUsuarios();
                call.enqueue(new Callback<Usuarios>() {
                    @Override
                    public void onResponse(Call<Usuarios> call, Response<Usuarios> response) {
                        System.out.println("Listar usuário - onResponse "+response.code()+" Mensagem "+response.message());

                        if(response.code() == 200) {
                            System.out.println("Sucesso, buscou pelo cep "+cep);
                            System.out.println("Status: "+response.body().status+" Resultado: "+response.body().resultado);

                            for (ObjUsuario obj: response.body().usuarios){
                                System.out.println(obj.nome_usuario);
                                System.out.println(obj.email_usuario);
                            }

                        } else {

                            Gson gson = new Gson();
                            try {
                                Endereco errorResponse = gson.fromJson(response.errorBody().string(), Endereco.class);
                                System.out.println(errorResponse.status);
                                System.out.println(errorResponse.resultado);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<Usuarios> call, Throwable t) {
                        System.out.println("onFailure");
                        try {
                            System.out.println(call.execute().body());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println(t);
                    }
                });

            }
        });

        novoUsuario = findViewById(R.id.novoUsuario);
        novoUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ICrudUsuario ObjUsuarios = ICrudUsuario.retrofit.create(ICrudUsuario.class);
                ObjUsuario obj = new ObjUsuario();
                obj.nome_usuario = "nome 1";
                obj.email_usuario = "e1@mail.com";
                obj.login_usuario = "nome 1";
                obj.senha_usuario = "40bd001563085fc35165329ea1ff5c5ecbdbbeef";

                final Call<Usuarios> call = ObjUsuarios.criarUsuario(obj);
                call.enqueue(new Callback<Usuarios>() {
                    @Override
                    public void onResponse(Call<Usuarios> call, Response<Usuarios> response) {
                        System.out.println("Novo usuário - onResponse "+response.code()+" Mensagem "+response.message());

                        if(response.code() == 200) {
                            System.out.println("Sucesso, buscou pelo cep "+cep);
                            System.out.println("Status: "+response.body().status+" Resultado: "+response.body().resultado);
                        } else {

                            Gson gson = new Gson();
                            try {
                                Endereco errorResponse = gson.fromJson(response.errorBody().string(), Endereco.class);
                                System.out.println(errorResponse.status);
                                System.out.println(errorResponse.resultado);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<Usuarios> call, Throwable t) {
                        System.out.println("onFailure");
                        try {
                            System.out.println(call.execute().body());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println(t);
                    }
                });

            }
        });


        editarUsuario = findViewById(R.id.editarUsuario);
        editarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ICrudUsuario ObjUsuarios = ICrudUsuario.retrofit.create(ICrudUsuario.class);
                ObjUsuario obj = new ObjUsuario();
                obj.nome_usuario = "nome 1 EDITADO";
                obj.email_usuario = "e1@maileditado.com";
                obj.login_usuario = "nome 1 EDITADO";
                obj.senha_usuario = "40bd001563085fc35165329ea1ff5c5ecbdbbeef";

                final Call<Usuarios> call = ObjUsuarios.editarUsuario(obj);
                call.enqueue(new Callback<Usuarios>() {
                    @Override
                    public void onResponse(Call<Usuarios> call, Response<Usuarios> response) {
                        System.out.println("Editar usuário - onResponse "+response.code()+" Mensagem "+response.message());

                        if(response.code() == 200) {
                            System.out.println("Sucesso, buscou pelo cep "+cep);
                            System.out.println("Status: "+response.body().status+" Resultado: "+response.body().resultado);
                        } else {

                            Gson gson = new Gson();
                            try {
                                Endereco errorResponse = gson.fromJson(response.errorBody().string(), Endereco.class);
                                System.out.println(errorResponse.status);
                                System.out.println(errorResponse.resultado);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<Usuarios> call, Throwable t) {
                        System.out.println("onFailure");
                        try {
                            System.out.println(call.execute().body());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println(t);
                    }
                });

            }
        });

    }
}
