package net.megamil.webservicesretrofit;

/**
 * Criado por Eduardo dos santos em 18/03/2018.
 * Megamil.net
 */

public class Endereco {

    int     status      = 0;
    String  resultado   = "";
    objEndereco endereco;


}

class objEndereco {
    String cep = "";
    String logradouro = "";
    String complemento = "";
    String bairro = "";
    String localidade = "";
    String uf = "";

    public String exibir(){
        return cep+" - "+logradouro+" - "+complemento+" - "+bairro+" - "+localidade+" - "+uf;
    }

}